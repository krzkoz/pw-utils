# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2018 Rob Herring <robh@kernel.org>
#!/bin/bash -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"

PATH=$DIR:$PATH

. $DIR/pw-utils.sh

my_pwclient() {
	local cmd=$1
	shift
	IFS=' '

	if [ -z "$dryrun" ]; then
		echo "$@" 1>&2
		command pwclient $cmd "$@"
	else
		echo "pwclient $cmd $@" >&2
	fi
}

check_duplicate_patches()
{
	echo "Processing duplicate patches..." >&2

	todo="$(pwclient list -a no -N 1000 -f '%{id} "%{state}" %{msgid} "%{name}"' | grep -E '^[0-9]+ "(New|Needs|Under)')"
	IFS='
'
	for subject in $(get_patch_subject "$todo" | sort -u); do
		matches=$(grep -F "$subject" <<< $todo | tac | tail -n +2)

		ids="$(get_patch_pw_id "$matches")"
		[ -z "$ids" ] && continue

		echo -e "Duplicates for \"$subject\":\n$ids" >&2
		IFS=' '
		my_pwclient update -s "Superseded" $ids
	done
}

check_already_reviewed()
{
	pwid=$1

	if grep -q -E "^(Acked|Reviewed)-by: Rob Herring"; then
		return 1
	fi

	return 0
}

check_patch()
{
	pwid=$1
	logfile="checkpatch-$pwid.log"

	scripts/checkpatch.pl --ignore FILE_PATH_CHANGES - > $logfile
	if [ $? -ne 0 ]; then
		echo "$pwid: checkpatch fail"
	else
		echo "$pwid: checkpatch pass"
		truncate -s0 $logfile
	fi
}

check_patch_update_pw()
{
	pwid=$1
	logfile="checkpatch-$pwid.log"

	[ -f $logfile ] || return

	if [ -s $logfile ]; then
		check=$(grep '^total:' $logfile)
		my_pwclient check-create -c checkpatch -s warning -d "$check" -u "$CI_JOB_URL/artifacts/file/$logfile" $pwid
		return 1
	else
		my_pwclient check-create -c checkpatch -s success $pwid
		rm $logfile
		return 0
	fi
}

apply_patch()
{
	pwid=$1
	logfile="applypatch-$pwid.log"

	# Keep only portions we care about testing
	temp_patch=$(filterdiff -v -i '*MAINTAINERS' -i '*Documentation/*' -i '*include/dt-bindings/*')

	git am -q -3 <<< $temp_patch 2> $logfile
	if [ $? -ne 0 ]; then
		git am --abort
		# Try again without MAINTAINERS
		temp_patch=$(filterdiff -v -i '*Documentation/*' -i '*include/dt-bindings/*' <<< $temp_patch)
		git am -q -3 <<< $temp_patch 2> $logfile
		if [ $? -ne 0 ]; then
			git am --abort
			return 1
		fi
	fi
	return 0
}

apply_patch_update_pw()
{
	pwid=$1
	logfile="applypatch-$pwid.log"

	[ -f $logfile ] || return 0

	if [ -s $logfile ]; then
		my_pwclient check-create -c patch-applied -s fail -d "build log" -u "$CI_JOB_URL/artifacts/file/$logfile" $pwid
		return 1
	else
		my_pwclient check-create -c patch-applied -s success $pwid
		rm $logfile
		return 0
	fi
}

baselinelog="base-$(git describe --always).log"

gen_baseline_log()
{
	[ -f "$baselinelog" ] && return

	echo "Generating baseline log..."
	make -s ARCH=arm allmodconfig
	make -s ARCH=arm refcheckdocs 2>&1 | tee doc-links-${baselinelog}
	make -O -j$(nproc) -s ARCH=arm DT_CHECKER_FLAGS=-m dt_binding_check 2>&1 | tee ${baselinelog}
}

dt_bindings_check_test_patch()
{
	pwid=$1
	IFS=' '
	logfile="build-$pwid.log"

	make -s ARCH=arm refcheckdocs 2>&1 | tee temp-doc-links.log
	make -O -j$(nproc) -s ARCH=arm DT_CHECKER_FLAGS=-m dt_binding_check 2>&1 | tee temp.log
	ret=${PIPESTATUS[0]}

	# filter out any expected warnings
	tmplog="$(filter-warnings ${baselinelog} temp.log)"
	echo "$tmplog" > temp.log

	echo "yamllint warnings/errors:" > $logfile
	sed -n -E '/\[(warning|error)\]/p' temp.log >> $logfile
	sed -i -E '/\[(warning|error)\]/d' temp.log
	echo "" >> $logfile

	echo "dtschema/dtc warnings/errors:" >> $logfile
	cat temp.log >> $logfile

	echo -e "\ndoc reference errors (make refcheckdocs):" >> $logfile
	echo "$(filter-warnings doc-links-${baselinelog} temp-doc-links.log)" >> $logfile

	rm temp.log temp-doc-links.log

	if grep -E '\.(yaml|dts|dtb):' $logfile; then
		ret=1
	fi

	if [ $ret -ne 0 ]; then
		git reset --hard $git_head
	else
		truncate -s0 $logfile
	fi
	return $ret
}

dt_bindings_check_update_pw()
{
	pwid=$1
	logfile="build-$pwid.log"

	[ -f $logfile ] || return 0

	if [ -s $logfile ]; then
		my_pwclient check-create -c dt-meta-schema -s fail -d "build log" -u "$CI_JOB_URL/artifacts/file/$logfile" $pwid
		return 1
	else
		my_pwclient check-create -c dt-meta-schema -s success $pwid
		rm $logfile
		return 0
	fi
}


dtbs_check_test_patch()
{
	pwid=$1
	IFS=' '
	logfile="build-dtbs-$pwid.log"
	rm -f $logfile

	# Disable options that cause make warnings
	cfg_opts="CONFIG_ARM64_ERRATUM_843419=n CONFIG_ARM64_USE_LSE_ATOMICS=n CONFIG_BROKEN_GAS_INST=n"

	for arch in arm arm64; do
		make -s mrproper
		make -s ARCH=$arch $cfg_opts allmodconfig
		make -O -j$(nproc) -s ARCH=$arch $cfg_opts DT_SCHEMA_FILES="$2" dtbs_check 2>&1 | tee -a $logfile

		# Filter dtc warnings out
		sed -i -E '/(: Warning|also defined at)/d' $logfile

		sed -n -i -E '/arch\/.*\.dtb: .*/p'
	done

	return 0
}

dtbs_check_update_pw()
{
	pwid=$1
	logfile="build-dtbs-$pwid.log"

	[ -f $logfile ] || return 0

	if [ -s $logfile ]; then
		my_pwclient check-create -c dtbs-check -s warning -d "build log" -u "$CI_JOB_URL/artifacts/file/$logfile" $pwid
		return 1
	else
		my_pwclient check-create -c dtbs-check -s success $pwid
		rm $logfile
		return 0
	fi
}

get_pending_patches()
{
	todo=$(pwclient list -a no -s New -f '%{id} "%{state}" %{msgid} "%{name}"')
	IFS='
'
	for id in ${todo}; do
		pwid=$(get_patch_pw_id "$id")

		patch=$(pwclient view $pwid)

		in_reply_to=$(echo "$patch" | sed -n 's/^In-Reply-To:.*\(<.*>\)/\1/p')
		echo "In-Reply-To: >>$in_reply_to<<" >&2
		# No In-Reply-To on 1st patch if no cover letter
		if [ -z "$in_reply_to" ]; then
			in_reply_to=$(echo "$patch" | sed -n 's/^Message-Id:.*\(<.*>\)/\1/p')
			echo "Message-Id: >>$in_reply_to<<" >&2
		fi

		[ -z "$old_in_reply_to" ] && old_in_reply_to="$in_reply_to"

		if [ "$in_reply_to" != "$old_in_reply_to" ]; then
			break
		fi

		echo "$id"
	done
}

update_pw_state()
{
	pwid=$1

	check_patch_update_pw $pwid
	apply_patch_update_pw $pwid
	dtbs_check_update_pw $pwid
	dt_bindings_check_update_pw $pwid

	# If no failures and already reviewed, move to N/A
	pwclient check-get -f '%{context}: %{state} %{patch_id} %{target_url}' $pwid | grep -q -F "dt-meta-schema: fail"
	if [ $? -ne 0 ]; then
		pwclient view $pwid | check_already_reviewed $pwid
		if [ $? -ne 0 ]; then
			echo "Already Reviewed: $pwid"
			my_pwclient update -s "Not Applicable" $pwid
			return
		fi
	fi

	echo "Needs Review: $pwid"
	my_pwclient update -s Needs $pwid
}

git_head=$(git rev-parse HEAD)

check_duplicate_patches

patches="$(get_pending_patches)"
if [ -z "${patches}" ]; then
	exit 0
fi

echo "$patches"

IFS=' '
my_pwclient update -s Under $(get_patch_pw_id "$patches")

gen_baseline_log

IFS='
'
for id in ${patches}; do
	pwid=$(get_patch_pw_id "$id")
	date
	echo $id
	rm -f *-$pwid.log

	patch=$(pwclient view $pwid)

	check_patch $pwid <<< "$patch"

	apply_patch $pwid <<< $patch
	applied=$?

	schema_files="$(diffstat -p0 -l <<< $patch | grep -o 'Documentation.*\.yaml$' | xargs)"
	if [ $applied -eq 0 -a -n "${schema_files}" ]; then
		dt_bindings_check_test_patch $pwid && \
		dtbs_check_test_patch $pwid "${schema_files}"
	fi

	# Wait til end to update state
	ids="${ids} ${pwid}"
done

# Update the state at the end in case we timeout
IFS=' '
for id in $ids; do
	update_pw_state $id
done

make -s ARCH=arm mrproper
