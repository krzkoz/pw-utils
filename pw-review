# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2018-2020 Rob Herring <robh@kernel.org>
#!/bin/bash -e

: ${PW_DELEGATE:="None"}
: ${PW_PROJECT:="devicetree-bindings"}
#: ${LORE_MAILLIST:="linux-devicetree"}
# Set to 1 if a Reviewed/Acked-by should *not* change state to N/A
: ${PW_SKIP_NA_ON_TAG:=0}
: ${LINUX_NEXT_BRANCH="next/master"}
: ${EMAIL:="$(git config user.name) <$(git config user.email)>"}

export EMAIL
export LORE_MAILLIST

[ -e ".git" ] || { echo "Must be run in a git tree!" >&2; exit 1; }

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"

export PATH=$DIR:$PATH

. $DIR/pw-utils.sh

filter='(Needs Review / ACK|New|Under Review)'
PW_REVIEW_PATH="$HOME/pw-review/$PW_PROJECT"
todofile="$PW_REVIEW_PATH/pw-todo"
updatefile="$PW_REVIEW_PATH/pw-updates"
LORE_MBOX_PATH="$PW_REVIEW_PATH/mbox"

[ ! -d "$LORE_MBOX_PATH" ] && mkdir -p "$LORE_MBOX_PATH"

pwclient() {
	local cmd=$1
	shift

	if [ -z "$off_line" ]; then
#		echo "$@" 1>&2
		command pwclient $cmd -p $PW_PROJECT "$@"
	else
		echo "pwclient $cmd -p $PW_PROJECT $@" >> "$updatefile"
	fi
}

pw_update_state() {
	local id
	# parameters: <state> <ids>
	pw_state="$1"
	local ids="$2"
	IFS=' '

	[ -z "$ids" ] && return

	pwclient update -s "$pw_state" $ids
	for id in $ids; do
		sed -i -e "/^$id/d" $todofile
	done
}

prompt_for_okay() {
	local temp
	echo -n "$1 [y/N]?"
	read -n 1 temp
	echo ""
	[ "$temp" = "y" ]
}

open_mail_msg() {
	local msgid=$1
	b4 mbox -o "${LORE_MBOX_PATH}" "$msgid" 2> /dev/null

	if mboxgrep -m mbox -H -i -G "message-id:\s*<$msgid>" "${LORE_MBOX_PATH}/${msgid}.mbx" | grep -q -E "^(Acked|Reviewed)-by: Rob Herring" - 2> /dev/null ; then
		prompt_for_okay "Already Acked, read thread " || return
	fi

	# mutt doesn't handle these correctly
	sed -i -e '/^To:\sunlisted-recipients.*/d' "${LORE_MBOX_PATH}/${msgid}.mbx"

	# Escape msg-id
	local mutt_msgid=$(sed -e 's/\+/\\\\\+/g' -e 's/=/\\=/g' -e 's/\%/\\\%/g' -e 's/\~/\\\~/g' -e 's/{/\\\\{/g' -e 's/}/\\\\}/g' <<< "$msgid")
	mutt -f "${LORE_MBOX_PATH}/${msgid}.mbx" -e "push <search>~i\ '${mutt_msgid}'<enter><enter>"
}

sort_todo_by_version() {
	for version in $(seq -s' ' 50 -1 2); do
		grep "\[.*[vV]${version}[],] *" $todofile
	done
        # Match all the v1 and untagged patches last
	grep -v '\[.*[vV][2-9].*[],]*]' $todofile
}

process_missing_review_tags() {
	local matched_ids=
	local id
	echo "Processing missing acks..."
	IFS='
'
	for id in $1; do
		subject="$(get_patch_subject "$id")"
		matched_ids="$(get_ids_for_subject "$subject" <<< "${list_all}")"

		IFS=' '
		for m in $matched_ids; do
			if pwclient view $m | grep -q -E "^(Acked|Reviewed)-by: Rob Herring" 2> /dev/null ; then

				echo -e "Missing review tag: $subject"
				read -n 1 temp

				msgid="$(get_patch_msgid "$id")"
				mail-readmsg "$msgid" | mail-add-acks-reply >> ~/postponed

				process_patch "$id"
				break
			fi
		done
	done
}

process_already_reviewed() {
	local id
	echo "Processing already reviewed..."
	IFS='
'
	for id in $1; do
		msgid=$(get_patch_msgid "$id")

		if b4 am -P_ -o - "${msgid}" 2> /dev/null | grep -q -E "^(Acked|Reviewed)-by: Krzysztof Kozlowski" ; then
			process_patch "$id"
		fi
	done
}

patch_in_next() {
	[ -n "${LINUX_NEXT_BRANCH}" ] || return

	local patchidfile=$PW_REVIEW_PATH/patch-ids-$(git rev-parse --short ${LINUX_NEXT_BRANCH}).txt
	local nextlogfile=$PW_REVIEW_PATH/nextlog-$(git rev-parse --short ${LINUX_NEXT_BRANCH}).txt

	if ! [ -f $nextlogfile ]; then
		eval "rm -rf $PW_REVIEW_PATH/nextlog-*.txt"

		git --no-pager log --oneline --no-merges --since='3 months' ${LINUX_NEXT_BRANCH} > $nextlogfile
	fi

	if ! [ -f $patchidfile ]; then
		eval "rm -rf $PW_REVIEW_PATH/patch-ids-*.txt"

		commits=$(git rev-list --reverse --no-merges --since='8 weeks' ${LINUX_NEXT_BRANCH} -- Documentation/devicetree/)
		echo "Updating $(wc -l <<< $commits) patch hashes from git..."
		for c in $commits; do
			hash=$(git show --pretty=email $c | python3 ~/proj/git/patchwork/patchwork/hasher.py)
			[ -n $hash ] && echo "$hash $c" >> "$patchidfile"
		done
		echo "Updating patch hashes done"
	fi

	subject=$(get_patch_subject "$1")

	grep -qF "${subject}" $nextlogfile && return

	patchid=$(cut -d' ' -f2 <<< "$1")
	grep -F "${patchid}" $patchidfile
}

process_applied_patches() {
	local id
	echo "Processing already applied patches..."
	IFS='
'
	for id in $1; do
		patch_in_next "$id" || continue

		echo -e "Patch already applied to linux-next: $subject"
		read -n 1 temp

		process_patch "$id"
	done
}

get_failing_checks() {
	local id
	checkname="$1"
	shift
	ids=$(get_patch_pw_id "$1")

	IFS=' '
	pwclient check-get -f '%{context}: %{state} %{patch_id} %{target_url}' $ids | grep "${checkname}: fail" | cut -d' ' -f3-
}

process_failing_checks() {
	local id
	echo "Processing patches failing checks..."

	IFS='
'
	for id in $(get_failing_checks dt-meta-schema "$1" | cut -d' ' -f1); do
		echo -e "Patch failed checks: $id"
		read -n 1 temp

		process_patch $(grep "^${id}" <<< "$1")
	done
}

check_patch() {
	local CHECKPATCH_ARGS="--ignore FILE_PATH_CHANGES --codespell --codespellfile /usr/lib/python3/dist-packages/codespell_lib/data/dictionary.txt"
	[ -n "$off_line" ] && return 0

	local id=$(echo "$1" | cut -f1 -d' ')
	pause=0

	msgid=$(get_patch_msgid "$*")
	patch="$(b4 am -P_ -o - ${msgid} 2> /dev/null)"
	patch_files=$(echo "${patch}" | diffstat -l -p1)
#	echo $patch_files
	if echo ${patch_files} | grep -q -E '(dt-bindings|devicetree)' ; then

		subject=$(echo "${patch}" | formail -cz -x Subject | sed -e 's/\[.*\] //')
		if ! [[ $subject = "dt-bindings: "* ]]; then
			echo "Subject should begin with 'dt-bindings: '"
			pause=1
		fi

		if echo ${patch_files} | grep -qv -E '(dt-bindings|devicetree|MAINTAINERS)' ; then
		 	echo Need to split patch
			pause=1
		fi
	fi
	IFS=' '
	echo "${patch}" | scripts/checkpatch.pl $CHECKPATCH_ARGS || pause=1
	[ $pause -ne 0 ] && (echo "Any key to continue..."; read -n 1 dummy)

	return 0
}

process_patch() {
	local id="$*"
	local pw_id=$(get_patch_pw_id "$id")

	[ $pw_id -lt $starting_pwid ] && return

	echo "$id"

	check_patch $id

	again=""; pw_state=""
	while [ -z "$again" -a -z "$pw_state" ]; do
		again=""; pw_state=""

		msgid=$(get_patch_msgid "$id")

		[ -z "$again" ] && open_mail_msg "$msgid"

		echo -ne "[E]dit again\n" \
			 "Next [P]atch\n" \
			 "Set patch state to:\n" \
			 "[C]hanges Requested/[U]nder Review/[A]ccepted/[N]ot Applicable/[S]uperseded/[R]ejected/R[F]C\n" \
			 "[Q]uit or <enter> key to skip state change: "
		read -n 1 state
		echo ""
		case "$state" in
		e|E)
			;;
		c|C)
			pw_state="Changes" # Requested
			;;
		u|U)
			pw_state="Under" # Review
			;;
		a|A)
			if ! b4 shazam -lsSt -P _ "$msgid"; then
				echo "*** Applying patch failed ***!"
				continue
			fi
			mail-readmsg "$msgid" | mail-accepted-reply >> ~/postponed
			(EDITOR=true mutt -p)
			pw_state="Accepted"
			;;
		n|N)
			echo -ne "[R]eviewed-by, [A]cked-by, or [N]o reply?: "
			read -n 1 state
			echo ""
			case "$state" in
			r|R)
				mail-readmsg "$msgid" | mail-reviewedby-reply >> ~/postponed
				(EDITOR=true mutt -p)
				;;
			a|A)
				mail-readmsg "$msgid" | mail-reviewedby-reply Acked-by >> ~/postponed
				(EDITOR=true mutt -p)
				;;
			*)
				;;
			esac
			[ $PW_SKIP_NA_ON_TAG -ne 1 ] && pw_state="Not" # Applicable
			again="no"
			;;
		s|S)
			pw_state="Superseded"
			;;
		r|R)
			pw_state="Rejected"
			;;
		F|f)
			pw_state="RFC"
			;;
		q|Q)
			exit 1
			;;
		p|P)
			again="no"
			;;

		*)
			;;
		esac

		if [ -n "${pw_state}" ]; then
			wait
			(pw_update_state "${pw_state}" "${pw_id}") &
		fi
	done

}

process_patches() {
	local ids="$*"
	local IFS='
'
	for id in $ids; do
		process_patch $id
	done
}

starting_pwid=0
while getopts "dfons:" opt
do
	case "$opt" in
	f)	skip_fail="1";;
	n)	new_only="1";;
	o)	off_line="1";;
	d)      date_sort="1";;
	s)      starting_pwid=$OPTARG;;
	[?])	echo "syntax: `basename $0` [-nodf] [-s <starting PW ID>] [PW IDs]"
		exit 1;;
	esac
done
shift $((OPTIND-1))
matching_ids="$*"

# Force offline mode if we don't have a connection to the server
[ -z $off_line ] && command pwclient states 2> /dev/null > /dev/null || (off_line="1"; echo "Offline mode")

if [ -z $off_line ]; then
	if [ -f $updatefile ] ; then
		prompt_for_okay "Pending PW commands found, run them " && \
			(sh -e $updatefile; mv $updatefile ${updatefile}.bak)
	fi

	echo "Getting patchwork data..."
	list_all=$(pwclient list -N 1000 -a no -f '%{id} %{hash} "%{state}" %{delegate} %{msgid} "%{name}"')
	grep -E "^[0-9]* [0-9a-f]* \"$filter\" $PW_DELEGATE" <<< "$list_all" > "$todofile"
fi

if ! [ -f "$todofile" ]; then
	exit 1;
fi

if [ -f $updatefile ]; then
	echo "Patchwork command file exists."
fi

if [ -z "$matching_ids" ]; then
	todo=$(cat "$todofile")

	[ -z "$off_line" ] && [ -z "$skip_fail" ] && process_failing_checks "$todo"

	process_applied_patches "$todo"

	process_already_reviewed "$todo"

	[ -z "$off_line" ] && process_missing_review_tags "$todo"
fi

if [ -n "$date_sort" ]; then
	ids=$(cat $todofile)
else
	ids=$(sort_todo_by_version)
fi


if [ -z "$off_line" ]; then
	IFS='
	'
	for id in $ids; do
		msgid=$(get_patch_msgid "$id")
		#echo "$msgid"

#		mail-readmsg "$msgid" | mail-auto-reply >> ~/postponed

	done
	mutt -p
fi

if [ -n "$matching_ids" ]; then
	tmp_patterns=$(mktemp)
	echo "${matching_ids}" | tr ' ' '\n' > ${tmp_patterns}

	ids=$(grep -f ${tmp_patterns} <<< "$ids")

	rm -f ${tmp_patterns}
fi

[ -n "$new_only" ] && ids="$(grep -E '^[0-9]* [0-9a-f]* \"(New|Needs)' <<< "$ids")"

echo "$(wc -l <<< $ids) patches to review"


process_patches $ids
